package com.revolut.interview.project.integration.suites.service;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.entities.Transfer;
import com.revolut.interview.project.exceptions.AccountCreationException;
import com.revolut.interview.project.exceptions.AccountNotFoundException;
import com.revolut.interview.project.exceptions.TransferBetweenAccountsException;
import com.revolut.interview.project.services.IAccountService;
import com.revolut.interview.project.services.impl.AccountService;
import com.revolut.interview.project.utils.HibernateUtil;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static com.revolut.interview.project.constants.TestConstants.INITIAL_BALANCE;
import static com.revolut.interview.project.constants.TestConstants.LESS_THAN_BALANCE_AMOUNT;

public class AccountServiceSuite {

    static IAccountService accountService = new AccountService();

    @BeforeClass
    public static void bootstrap() {
        HibernateUtil.getSessionFactory();
    }

    @Test
    public void createAccount() {
        Account account = accountService.createAccount(BigDecimal.ZERO);
        Assert.assertNotNull(account);
        Assert.assertNotNull(account.getUuid());
    }

    @Test(expected = AccountCreationException.class)
    public void createAccountError() {
        accountService.createAccount(null);
    }

    @Test
    public void findAccount() {

        Account account = accountService.createAccount(BigDecimal.ZERO);

        Account queryAccount = accountService.findAccount(account.getUuid());

        Assert.assertNotNull(queryAccount);

    }

    @Test(expected = AccountNotFoundException.class)
    public void findAccountError() {

        String uuid = UUID.randomUUID().toString();

        Account queryAccount = accountService.findAccount(uuid);

        Assert.assertNotNull(queryAccount);

    }

    @Test
    public void transferBetweenDifferentAccounts() {

        Account source = accountService.createAccount(INITIAL_BALANCE);

        Account target = accountService.createAccount(INITIAL_BALANCE);

        String sourceUUID = source.getUuid();

        String targetUUID = target.getUuid();

        Transfer transfer = accountService.transfer(sourceUUID, targetUUID, LESS_THAN_BALANCE_AMOUNT);

        Assert.assertNotNull(transfer);

        Assert.assertEquals(transfer.amount, LESS_THAN_BALANCE_AMOUNT);

    }

    @Test(expected = TransferBetweenAccountsException.class)
    public void transferBetweenSameAccount() {

        Account source = accountService.createAccount(INITIAL_BALANCE);

        String sourceUUID = source.getUuid();

        accountService.transfer(sourceUUID, sourceUUID, LESS_THAN_BALANCE_AMOUNT);

    }

    @Test(expected = TransferBetweenAccountsException.class)
    public void transferFromUnkownAccount() {

        Account target = accountService.createAccount(INITIAL_BALANCE);

        String uuid = UUID.randomUUID().toString();

        accountService.transfer(uuid, target.getUuid(), LESS_THAN_BALANCE_AMOUNT);

    }

    @Test(expected = TransferBetweenAccountsException.class)
    public void transferToUnkownAccount() {

        Account source = accountService.createAccount(INITIAL_BALANCE);

        String uuid = UUID.randomUUID().toString();

        accountService.transfer(source.getUuid(), uuid, LESS_THAN_BALANCE_AMOUNT);

    }

}
