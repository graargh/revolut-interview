package com.revolut.interview.project.integration.suites.database;

import com.revolut.interview.project.exceptions.TransactionActiveException;
import com.revolut.interview.project.exceptions.TransactionNotFoundException;
import com.revolut.interview.project.services.AbstractService;
import com.revolut.interview.project.utils.HibernateUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManager;

public class DatabaseSuite extends AbstractService {

    @BeforeClass
    public static void bootstrap() {
        HibernateUtil.getSessionFactory();
    }

    @After
    public void after() {

        if (transaction != null && transaction.isActive()) {
            transaction.rollback();
        }

    }

    @Test
    public void testEntityManagerCreation() {

        EntityManager entityManager = getEntityManager();

        Assert.assertNotNull(entityManager);

        entityManager.close();

    }

    @Test
    public void testStartTransaction() {
        startTx();
    }

    @Test(expected = TransactionActiveException.class)
    public void testStartTransactionTwice() {
        startTx();
        startTx();
    }

    @Test
    public void commitTransaction() {
        startTx();
        commitTx();
    }

    @Test(expected = TransactionNotFoundException.class)
    public void commitTransactionWithoutTransaction() {
        commitTx();
    }

    @Test
    public void rollbackTransaction() {
        startTx();
        rollbackTx();
    }

    @Test(expected = TransactionNotFoundException.class)
    public void rollbackTransactionWithoutTransaction() {
        rollbackTx();
    }

}
