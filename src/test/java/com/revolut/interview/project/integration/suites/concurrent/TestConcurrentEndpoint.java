package com.revolut.interview.project.integration.suites.concurrent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;
import com.revolut.interview.project.Runner;
import com.revolut.interview.project.constants.ApplicationConstants;
import com.revolut.interview.project.entities.Account;
import org.junit.*;
import spark.Spark;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import static com.revolut.interview.project.constants.TestConstants.*;

public class TestConcurrentEndpoint {

    static CountDownLatch countDownLatch = new CountDownLatch(CONCURRENT_NUMBER_TRANSACTIONS);

    AsyncHttpClient httpClient = new AsyncHttpClient();

    Gson gson = new GsonBuilder().create();

    static Account source;

    static String sourceUuid;

    static Account target;

    private String getURL(String input) {
        return String.format("http://localhost:%s%s", ApplicationConstants.DEFAULT_PORT, input);
    }

    @BeforeClass
    public static void bootstrap() {
        Runner.main(new String[]{});
    }

    @AfterClass
    public static void tearDown() {
        Spark.stop();
    }

    @Before
    public void before() throws ExecutionException, InterruptedException {

        source = createAccount(CONCURRENT_INITIAL_BALANCE);

        sourceUuid = source.getUuid();

        target = createAccount(BigDecimal.ZERO);

    }

    private Account createAccount(BigDecimal balance) {

        try {

            String url = getURL(ApplicationConstants.ACCOUNT_ADD_ENDPOINT)
                    .replace(":balance", balance.toString());

            Response response = httpClient.preparePut(url).execute().get();

            String body = response.getResponseBody();

            return gson.fromJson(body, Account.class);

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    @Test
    public void testConcurrentTransferService() throws ExecutionException, InterruptedException, IOException {

        for (int i = 0; i < CONCURRENT_NUMBER_TRANSACTIONS; i++) {

            TransferThread transferThread = new TransferThread(source, target, LESS_THAN_BALANCE_AMOUNT);

            transferThread.run();

        }

        countDownLatch.await();

        String url = getURL(ApplicationConstants.ACCOUNT_FIND_ENDPOINT)
                .replace(":uuid", sourceUuid);

        Response response = httpClient.prepareGet(url).execute().get();

        String body = response.getResponseBody();

        Account account = gson.fromJson(body, Account.class);

        BigDecimal expectedBalance = LESS_THAN_BALANCE_AMOUNT.multiply(new BigDecimal(CONCURRENT_NUMBER_TRANSACTIONS));

        Assert.assertEquals(0, expectedBalance.compareTo(account.balance));

    }

    /**
     * Helper thread class created to invoke the transfer endpoint.
     */
    private class TransferThread extends Thread {

        private final Account source;

        private final Account target;

        private final BigDecimal amount;

        public TransferThread(Account source, Account target, BigDecimal amount) {
            this.source = source;
            this.target = target;
            this.amount = amount;
        }

        @Override
        public void run() {

            try {

                String url = getURL(ApplicationConstants.ACCOUNT_TRANSFER_ENDPOINT)
                        .replace(":sourceUUID", source.getUuid())
                        .replace(":targetUUID", target.getUuid())
                        .replace(":amount", amount.toString());

                httpClient.preparePost(url).execute().get();

                countDownLatch.countDown();

            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }

        }

    }

}
