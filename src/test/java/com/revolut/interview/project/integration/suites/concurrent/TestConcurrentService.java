package com.revolut.interview.project.integration.suites.concurrent;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.services.IAccountService;
import com.revolut.interview.project.services.impl.AccountService;
import com.revolut.interview.project.utils.HibernateUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import static com.revolut.interview.project.constants.TestConstants.*;

public class TestConcurrentService {

    static CountDownLatch countDownLatch = new CountDownLatch(CONCURRENT_NUMBER_TRANSACTIONS);

    static IAccountService accountService = new AccountService();

    static Account source;

    static String sourceUuid;

    static Account target;

    @BeforeClass
    public static void bootstrap() {
        HibernateUtil.getSessionFactory();
    }

    @Before
    public void before() {

        source = accountService.createAccount(CONCURRENT_INITIAL_BALANCE);

        sourceUuid = source.getUuid();

        target = accountService.createAccount(BigDecimal.ZERO);

    }

    @Test
    public void testConcurrentTransferService() throws ExecutionException, InterruptedException {

        for (int i = 0; i < CONCURRENT_NUMBER_TRANSACTIONS; i++) {

            TransferThread transferThread = new TransferThread(source, target, LESS_THAN_BALANCE_AMOUNT);

            transferThread.run();

        }

        countDownLatch.await();

        Account account = accountService.findAccount(sourceUuid);

        BigDecimal expectedBalance = LESS_THAN_BALANCE_AMOUNT.multiply(new BigDecimal(CONCURRENT_NUMBER_TRANSACTIONS));

        Assert.assertEquals(0, expectedBalance.compareTo(account.balance));

    }

    /**
     * Helper thread class created to invoke the transfer service.
     */
    private class TransferThread extends Thread {

        private final Account source;

        private final Account target;

        private final BigDecimal amount;

        public TransferThread(Account source, Account target, BigDecimal amount) {
            this.source = source;
            this.target = target;
            this.amount = amount;
        }

        @Override
        public void run() {

            String sourceUuid = source.getUuid();

            String targetUuid = target.getUuid();

            accountService.transfer(sourceUuid, targetUuid, amount);

            countDownLatch.countDown();

        }

    }

}
