package com.revolut.interview.project.integration.suites.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Response;
import com.revolut.interview.project.Runner;
import com.revolut.interview.project.constants.ApplicationConstants;
import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.entities.Transfer;
import com.revolut.interview.project.exceptions.AccountCreationException;
import com.revolut.interview.project.exceptions.AccountNotFoundException;
import com.revolut.interview.project.exceptions.TransferBetweenAccountsException;
import com.revolut.interview.project.services.impl.AccountService;
import com.revolut.interview.project.utils.NumberUtil;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static com.revolut.interview.project.constants.TestConstants.INITIAL_BALANCE;
import static com.revolut.interview.project.constants.TestConstants.LESS_THAN_BALANCE_AMOUNT;

public class ServerSuite {

    AsyncHttpClient httpClient = new AsyncHttpClient();

    Gson gson = new GsonBuilder().create();

    AccountService accountService = new AccountService();

    @BeforeClass
    public static void bootstrap() {
        Runner.main(new String[]{});
    }

    @AfterClass
    public static void tearDown() {
        Spark.stop();
    }

    private String getURL(String input) {
        return String.format("http://localhost:%s%s", ApplicationConstants.DEFAULT_PORT, input);
    }

    @Test
    public void testHealthCheck() throws ExecutionException, InterruptedException, IOException {

        String url = getURL(ApplicationConstants.HEALTH_CHECK_ENDPOINT);

        Response response = httpClient.prepareGet(url).execute().get();

        String body = response.getResponseBody();

        Assert.assertEquals(body, ApplicationConstants.HEALTH_CHECK_RESPONSE);

    }

    @Test
    public void testCreateAccount() throws ExecutionException, InterruptedException, IOException {

        String url = getURL(ApplicationConstants.ACCOUNT_ADD_ENDPOINT);

        String balance = LESS_THAN_BALANCE_AMOUNT.toString();

        url = url.replace(":balance", balance);

        Response response = httpClient.preparePut(url).execute().get();

        String body = response.getResponseBody();

        Account account = gson.fromJson(body, Account.class);

        Assert.assertNotNull(account);

        Assert.assertEquals(account.balance, NumberUtil.getBigDecimal(balance));

    }

    @Test
    public void testCreateAccountExeception() throws ExecutionException, InterruptedException, IOException {

        String url = getURL(ApplicationConstants.ACCOUNT_ADD_ENDPOINT);

        String balance = "-5";

        url = url.replace(":balance", balance);

        Response response = httpClient.preparePut(url).execute().get();

        String body = response.getResponseBody();

        AccountCreationException exception = gson.fromJson(body, AccountCreationException.class);

        Assert.assertNotNull(exception);

        Assert.assertEquals(1L, exception.code);

    }

    @Test
    public void testFindAccount() throws ExecutionException, InterruptedException, IOException {

        Account account = accountService.createAccount(INITIAL_BALANCE);

        String url = getURL(ApplicationConstants.ACCOUNT_FIND_ENDPOINT);

        String uuid = account.getUuid();

        url = url.replace(":uuid", uuid);

        Response response = httpClient.prepareGet(url).execute().get();

        String body = response.getResponseBody();

        account = gson.fromJson(body, Account.class);

        Assert.assertNotNull(account);

        Assert.assertEquals(account.getUuid(), uuid);

    }

    @Test
    public void testFindAccountException() throws ExecutionException, InterruptedException, IOException {

        String url = getURL(ApplicationConstants.ACCOUNT_FIND_ENDPOINT);

        url = url.replace(":uuid", UUID.randomUUID().toString());

        Response response = httpClient.prepareGet(url).execute().get();

        String body = response.getResponseBody();

        AccountNotFoundException accountNotFoundException = gson.fromJson(body, AccountNotFoundException.class);

        Assert.assertNotNull(accountNotFoundException);

        Assert.assertEquals(2L, accountNotFoundException.code);

    }

    @Test
    public void testTransferAccount() throws ExecutionException, InterruptedException, IOException {

        Account source = accountService.createAccount(INITIAL_BALANCE);

        Account target = accountService.createAccount(INITIAL_BALANCE);

        String url = getURL(ApplicationConstants.ACCOUNT_TRANSFER_ENDPOINT);

        String amount = LESS_THAN_BALANCE_AMOUNT.toString();

        url = url.replace(":sourceUUID", source.getUuid())
                .replace(":targetUUID", target.getUuid())
                .replace(":amount", amount);

        Response response = httpClient.preparePost(url).execute().get();

        String body = response.getResponseBody();

        Transfer transfer = gson.fromJson(body, Transfer.class);

        Assert.assertNotNull(transfer);

        BigDecimal expectedBalance = INITIAL_BALANCE.subtract(LESS_THAN_BALANCE_AMOUNT);

        Assert.assertTrue(expectedBalance.compareTo(transfer.amount) == 0);

    }

    @Test
    public void testTransferAccountInvalidSource() throws ExecutionException, InterruptedException, IOException {

        Account account = accountService.createAccount(INITIAL_BALANCE);

        String url = getURL(ApplicationConstants.ACCOUNT_TRANSFER_ENDPOINT);

        String amount = LESS_THAN_BALANCE_AMOUNT.toString();

        url = url.replace(":sourceUUID", UUID.randomUUID().toString())
                .replace(":targetUUID", account.getUuid())
                .replace(":amount", amount);

        Response response = httpClient.preparePost(url).execute().get();

        String body = response.getResponseBody();

        TransferBetweenAccountsException exception = gson.fromJson(body, TransferBetweenAccountsException.class);

        Assert.assertNotNull(exception);

        Assert.assertEquals(10L, exception.code);

    }

    @Test
    public void testTransferAccountInvalidTarget() throws ExecutionException, InterruptedException, IOException {

        Account account = accountService.createAccount(INITIAL_BALANCE);

        String amount = LESS_THAN_BALANCE_AMOUNT.toString();

        String url = getURL(ApplicationConstants.ACCOUNT_TRANSFER_ENDPOINT)
                .replace(":sourceUUID", account.getUuid())
                .replace(":targetUUID", UUID.randomUUID().toString())
                .replace(":amount", amount);

        Response response = httpClient.preparePost(url).execute().get();

        String body = response.getResponseBody();

        TransferBetweenAccountsException exception = gson.fromJson(body, TransferBetweenAccountsException.class);

        Assert.assertNotNull(exception);

        Assert.assertEquals(10L, exception.code);

    }

}
