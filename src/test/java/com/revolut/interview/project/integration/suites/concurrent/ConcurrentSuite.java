package com.revolut.interview.project.integration.suites.concurrent;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestConcurrentService.class,
        TestConcurrentEndpoint.class
})
public class ConcurrentSuite {

}
