package com.revolut.interview.project.unit.suites.deposit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestDepositNullAmount.class,
        TestDepositNegativeAmount.class,
        TestDepositPositiveAmount.class
})
public class DepositSuite {

}
