package com.revolut.interview.project.unit.suites.transfer;

import com.revolut.interview.project.entities.Account;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static com.revolut.interview.project.constants.TestConstants.INITIAL_BALANCE;
import static com.revolut.interview.project.constants.TestConstants.LESS_THAN_BALANCE_AMOUNT;

public class TestTransferToDifferentAccount {

    @Test
    public void test() {

        Account source = new Account(INITIAL_BALANCE);

        Account target = new Account(INITIAL_BALANCE);

        source.transfer(LESS_THAN_BALANCE_AMOUNT, target);

        BigDecimal sourceExpectedBalance = INITIAL_BALANCE.subtract(LESS_THAN_BALANCE_AMOUNT);

        Assert.assertEquals(source.balance, sourceExpectedBalance);

        BigDecimal targetExpectedBalance = INITIAL_BALANCE.add(LESS_THAN_BALANCE_AMOUNT);

        Assert.assertEquals(target.balance, targetExpectedBalance);

    }

}
