package com.revolut.interview.project.unit.suites.withdraw;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestWithdrawNullAmount.class,
        TestWithdrawNegativeAmount.class,
        TestWithdrawLessThanBalance.class,
        TestWithdrawGreaterThanBalance.class
})
public class WithdrawSuite {

}
