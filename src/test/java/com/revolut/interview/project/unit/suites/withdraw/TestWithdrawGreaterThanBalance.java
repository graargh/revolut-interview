package com.revolut.interview.project.unit.suites.withdraw;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.exceptions.InsufficientFundsException;
import org.junit.Test;

import static com.revolut.interview.project.constants.TestConstants.GREATER_THAN_BALANCE_AMOUNT;
import static com.revolut.interview.project.constants.TestConstants.INITIAL_BALANCE;

public class TestWithdrawGreaterThanBalance {

    @Test(expected = InsufficientFundsException.class)
    public void test() {

        Account account = new Account(INITIAL_BALANCE);

        account.withdraw(GREATER_THAN_BALANCE_AMOUNT);

    }

}
