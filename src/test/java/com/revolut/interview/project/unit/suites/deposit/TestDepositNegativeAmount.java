package com.revolut.interview.project.unit.suites.deposit;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.exceptions.NegativeAmountException;
import org.junit.Test;

import static com.revolut.interview.project.constants.TestConstants.INITIAL_BALANCE;
import static com.revolut.interview.project.constants.TestConstants.NEGATIVE_AMOUNT;

public class TestDepositNegativeAmount {

    @Test(expected = NegativeAmountException.class)
    public void test() {

        Account account = new Account(INITIAL_BALANCE);

        account.deposit(NEGATIVE_AMOUNT);

    }

}
