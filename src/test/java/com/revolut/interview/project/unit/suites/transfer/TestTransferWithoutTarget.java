package com.revolut.interview.project.unit.suites.transfer;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.exceptions.InvalidTransferException;
import org.junit.Test;

import static com.revolut.interview.project.constants.TestConstants.INITIAL_BALANCE;
import static com.revolut.interview.project.constants.TestConstants.LESS_THAN_BALANCE_AMOUNT;

public class TestTransferWithoutTarget {

    @Test(expected = InvalidTransferException.class)
    public void test() {

        Account source = new Account(INITIAL_BALANCE);

        Account target = null;

        source.transfer(LESS_THAN_BALANCE_AMOUNT, target);

    }

}
