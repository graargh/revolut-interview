package com.revolut.interview.project.unit.suites.withdraw;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.exceptions.RequiredAmountException;
import org.junit.Test;

import static com.revolut.interview.project.constants.TestConstants.INITIAL_BALANCE;
import static com.revolut.interview.project.constants.TestConstants.NULL_AMOUNT;

public class TestWithdrawNullAmount {

    @Test(expected = RequiredAmountException.class)
    public void test() {

        Account account = new Account(INITIAL_BALANCE);

        account.withdraw(NULL_AMOUNT);

    }

}
