package com.revolut.interview.project.unit.suites.deposit;

import com.revolut.interview.project.entities.Account;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static com.revolut.interview.project.constants.TestConstants.INITIAL_BALANCE;
import static com.revolut.interview.project.constants.TestConstants.LESS_THAN_BALANCE_AMOUNT;

public class TestDepositPositiveAmount {

    @Test
    public void test() {

        Account account = new Account(INITIAL_BALANCE);

        account.deposit(LESS_THAN_BALANCE_AMOUNT);

        BigDecimal expectedBalance = INITIAL_BALANCE.add(LESS_THAN_BALANCE_AMOUNT);

        Assert.assertEquals(account.balance, expectedBalance);

    }

}
