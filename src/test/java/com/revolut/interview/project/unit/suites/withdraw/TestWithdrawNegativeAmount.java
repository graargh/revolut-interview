package com.revolut.interview.project.unit.suites.withdraw;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.exceptions.NegativeAmountException;
import org.junit.Test;

import static com.revolut.interview.project.constants.TestConstants.INITIAL_BALANCE;
import static com.revolut.interview.project.constants.TestConstants.NEGATIVE_AMOUNT;

public class TestWithdrawNegativeAmount {

    @Test(expected = NegativeAmountException.class)
    public void test() {

        Account account = new Account(INITIAL_BALANCE);

        account.withdraw(NEGATIVE_AMOUNT);

    }

}
