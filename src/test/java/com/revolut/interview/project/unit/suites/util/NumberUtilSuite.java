package com.revolut.interview.project.unit.suites.util;

import com.revolut.interview.project.exceptions.InvalidAmountFormatException;
import com.revolut.interview.project.utils.NumberUtil;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class NumberUtilSuite {

    @Test
    public void testValid() {

        BigDecimal bigDecimal = NumberUtil.getBigDecimal("5");

        Assert.assertNotNull(bigDecimal);

    }

    @Test(expected = InvalidAmountFormatException.class)
    public void testInvalid() {
        NumberUtil.getBigDecimal("not a number");
    }

}
