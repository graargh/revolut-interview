package com.revolut.interview.project.unit.suites.transfer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestTransferWithoutTarget.class,
        TestTransferToSameAccount.class,
        TestTransferToDifferentAccount.class
})
public class TransferSuite {

}
