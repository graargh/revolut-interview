package com.revolut.interview.project;

import com.revolut.interview.project.integration.suites.concurrent.ConcurrentSuite;
import com.revolut.interview.project.integration.suites.database.DatabaseSuite;
import com.revolut.interview.project.integration.suites.server.ServerSuite;
import com.revolut.interview.project.integration.suites.service.AccountServiceSuite;
import com.revolut.interview.project.unit.suites.deposit.DepositSuite;
import com.revolut.interview.project.unit.suites.transfer.TransferSuite;
import com.revolut.interview.project.unit.suites.util.NumberUtilSuite;
import com.revolut.interview.project.unit.suites.withdraw.WithdrawSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ServerSuite.class,
        DepositSuite.class,
        TransferSuite.class,
        WithdrawSuite.class,
        DatabaseSuite.class,
        ConcurrentSuite.class,
        NumberUtilSuite.class,
        AccountServiceSuite.class
})
public class InterviewSuites {
}
