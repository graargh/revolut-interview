package com.revolut.interview.project.constants;

import java.math.BigDecimal;

public abstract class TestConstants {

    public static final Integer CONCURRENT_NUMBER_TRANSACTIONS = 1000;

    public static final BigDecimal INITIAL_BALANCE = new BigDecimal(10);

    public static final BigDecimal NULL_AMOUNT = null;

    public static final BigDecimal NEGATIVE_AMOUNT = new BigDecimal(-5);

    public static final BigDecimal LESS_THAN_BALANCE_AMOUNT = new BigDecimal(5);

    public static final BigDecimal GREATER_THAN_BALANCE_AMOUNT = new BigDecimal(25);

    public static final BigDecimal CONCURRENT_INITIAL_BALANCE = new BigDecimal(10000);

}
