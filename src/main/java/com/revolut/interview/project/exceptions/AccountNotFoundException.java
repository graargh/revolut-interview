package com.revolut.interview.project.exceptions;

public class AccountNotFoundException extends ExposableException {

    public AccountNotFoundException() {
        super(2L, "Account not found");
    }

}
