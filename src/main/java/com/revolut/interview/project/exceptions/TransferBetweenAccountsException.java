package com.revolut.interview.project.exceptions;

public class TransferBetweenAccountsException extends ExposableException {

    public TransferBetweenAccountsException() {
        super(10L, "An error has ocurred when transfering between accounts");
    }

}
