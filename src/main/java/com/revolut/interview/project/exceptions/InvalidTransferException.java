package com.revolut.interview.project.exceptions;

public class InvalidTransferException extends ExposableException {

    public InvalidTransferException() {
        super(5L, "Transfer to/between the same account is not allowed");
    }

}
