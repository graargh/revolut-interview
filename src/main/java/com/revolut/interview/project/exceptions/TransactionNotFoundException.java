package com.revolut.interview.project.exceptions;

public class TransactionNotFoundException extends ExposableException {

    public TransactionNotFoundException() {
        super(9L, "Transaction was not found");
    }

}
