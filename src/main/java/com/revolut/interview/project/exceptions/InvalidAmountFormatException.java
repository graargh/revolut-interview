package com.revolut.interview.project.exceptions;

public class InvalidAmountFormatException extends ExposableException {

    public InvalidAmountFormatException() {
        super(4L, "An error has ocurred when creating the account");
    }

}
