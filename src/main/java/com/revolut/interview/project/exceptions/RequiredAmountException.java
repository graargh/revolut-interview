package com.revolut.interview.project.exceptions;

public class RequiredAmountException extends ExposableException {

    public RequiredAmountException() {
        super(7L, "Amount is a required parameter");
    }

}
