package com.revolut.interview.project.exceptions;

public class InsufficientFundsException extends ExposableException {

    public InsufficientFundsException() {
        super(3L, "Insufficient funds");
    }

}
