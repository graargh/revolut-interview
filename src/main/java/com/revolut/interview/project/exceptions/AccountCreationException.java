package com.revolut.interview.project.exceptions;

public class AccountCreationException extends ExposableException {

    public AccountCreationException() {
        super(1L, "An error has ocurred when creating the account");
    }

}
