package com.revolut.interview.project.exceptions;

public class TransactionActiveException extends ExposableException {

    public TransactionActiveException() {
        super(8L, "Transaction is already active");
    }

}
