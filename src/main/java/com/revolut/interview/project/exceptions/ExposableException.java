package com.revolut.interview.project.exceptions;

import com.google.gson.annotations.Expose;

public abstract class ExposableException extends RuntimeException {

    @Expose
    public String message;

    @Expose
    public long code;

    public ExposableException(long code, String message) {
        this.code = code;
        this.message = message;
    }

}
