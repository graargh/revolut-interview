package com.revolut.interview.project.exceptions;

public class NegativeAmountException extends ExposableException {

    public NegativeAmountException() {
        super(6L, "Amount must be a positive number");
    }

}
