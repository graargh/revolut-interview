package com.revolut.interview.project;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.revolut.interview.project.configuration.Application;
import com.revolut.interview.project.configuration.DefaultModule;

/**
 * Default entry point for starting the application.
 * Declares a Guice module for dependency injection.
 */
public class Runner {

    public static void main(String[] args) {

        Module module = new DefaultModule();

        Injector injector = Guice.createInjector(module);

        injector.getInstance(Application.class).initialize(injector);

    }

}
