package com.revolut.interview.project.entities;

import com.google.gson.annotations.Expose;
import com.revolut.interview.project.exceptions.InsufficientFundsException;
import com.revolut.interview.project.exceptions.InvalidTransferException;
import com.revolut.interview.project.exceptions.NegativeAmountException;
import com.revolut.interview.project.exceptions.RequiredAmountException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Main entity of the project. Used to store/send money.
 */
@Entity
@Table
public class Account extends AbstractEntity {

    @Expose
    @Column(nullable = false)
    public BigDecimal balance;

    public Account() {
        this.balance = BigDecimal.ZERO;
    }

    public Account(BigDecimal balance) {
        validateAmount(balance);
        this.balance = balance;
    }

    public void withdraw(BigDecimal amount) {

        validateAmount(amount);

        if (amount.compareTo(balance) > 0) {
            throw new InsufficientFundsException();
        }

        balance = balance.subtract(amount);

    }

    public void deposit(BigDecimal amount) {

        validateAmount(amount);

        balance = balance.add(amount);

    }

    private void validateAmount(BigDecimal amount) {

        if (amount == null) {
            throw new RequiredAmountException();
        }

        if (BigDecimal.ZERO.compareTo(amount) > 0) {
            throw new NegativeAmountException();
        }

    }

    public Transfer transfer(BigDecimal amount, Account account) {

        if (account == null || this.equals(account)) {
            throw new InvalidTransferException();
        }

        validateAmount(amount);

        withdraw(amount);

        account.deposit(amount);

        return new Transfer(this, account, amount);

    }

}
