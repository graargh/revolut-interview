package com.revolut.interview.project.entities;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@MappedSuperclass
public class AbstractEntity {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Expose
    @Column(nullable = false, unique = true)
    private final String uuid = UUID.randomUUID().toString();

    public String getUuid() {
        return uuid;
    }

}
