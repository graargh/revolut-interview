package com.revolut.interview.project.entities;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created to register the history of transactions.
 */
@Entity
@Table
public class Transfer extends AbstractEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    public Account source;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    public Account target;

    @Expose
    @Column(nullable = false)
    public BigDecimal amount;

    private Transfer() {

    }

    public Transfer(Account source, Account target, BigDecimal amount) {
        this.source = source;
        this.target = target;
        this.amount = amount;
    }

}
