package com.revolut.interview.project.configuration;

import com.google.inject.AbstractModule;
import com.revolut.interview.project.services.IAccountService;
import com.revolut.interview.project.services.impl.AccountService;

/**
 * Configuration entry point for dependency injection.
 */
public class DefaultModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IAccountService.class).to(AccountService.class);
    }

}
