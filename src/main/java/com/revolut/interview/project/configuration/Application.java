package com.revolut.interview.project.configuration;

import com.google.inject.Injector;
import com.revolut.interview.project.constants.ApplicationConstants;
import com.revolut.interview.project.controllers.AccountController;
import com.revolut.interview.project.controllers.ExceptionController;
import com.revolut.interview.project.controllers.HealthCheckController;
import com.revolut.interview.project.utils.HibernateUtil;
import spark.Spark;

/**
 * Configuration entry point for the application.
 */
public class Application {

    /**
     * Initialize the server and configure the controllers.
     *
     * @param injector {@link Injector} instance
     */
    public void initialize(Injector injector) {

        HibernateUtil.getSessionFactory();

        Spark.port(ApplicationConstants.DEFAULT_PORT);

        injector.getInstance(AccountController.class);

        injector.getInstance(ExceptionController.class);

        injector.getInstance(HealthCheckController.class);

    }

}
