package com.revolut.interview.project.controllers;

import spark.Spark;

/**
 * Generic controller for returning exceptions in a friendly-like manner.
 */
public class ExceptionController extends AbstractController {

    public ExceptionController() {

        Spark.exception(Exception.class, ((exception, request, response) -> {

            String responseJson = json(exception);

            response.status(500);

            response.body(responseJson);

        }));

    }

}
