package com.revolut.interview.project.controllers;

import spark.Spark;

import static com.revolut.interview.project.constants.ApplicationConstants.HEALTH_CHECK_ENDPOINT;
import static com.revolut.interview.project.constants.ApplicationConstants.HEALTH_CHECK_RESPONSE;

/**
 * Endpoint for checking service's health.
 */
public class HealthCheckController extends AbstractController {

    public HealthCheckController() {

        Spark.get(HEALTH_CHECK_ENDPOINT, (request, response) -> HEALTH_CHECK_RESPONSE);

    }

}
