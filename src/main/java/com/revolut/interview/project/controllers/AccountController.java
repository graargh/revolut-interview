package com.revolut.interview.project.controllers;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.entities.Transfer;
import com.revolut.interview.project.services.IAccountService;
import com.revolut.interview.project.services.impl.AccountService;
import com.revolut.interview.project.utils.NumberUtil;
import spark.Request;
import spark.Spark;

import java.math.BigDecimal;

import static com.revolut.interview.project.constants.ApplicationConstants.*;

/**
 * Main controller for the project. Define endpoints for account operations.
 */
public class AccountController extends AbstractController {

    public AccountController() {

        Spark.put(ACCOUNT_ADD_ENDPOINT, (request, response) -> {

            IAccountService accountService = new AccountService();

            BigDecimal balance = getBigDecimal(request, "balance");

            Account account = accountService.createAccount(balance);

            return json(account);

        });

        Spark.get(ACCOUNT_FIND_ENDPOINT, (request, response) -> {

            IAccountService accountService = new AccountService();

            String uuid = request.params("uuid");

            Account account = accountService.findAccount(uuid);

            return json(account);

        });

        Spark.post(ACCOUNT_TRANSFER_ENDPOINT, (request, response) -> {

            IAccountService accountService = new AccountService();

            String sourceUUID = request.params("sourceUUID");

            String targetUUID = request.params("targetUUID");

            BigDecimal amount = getBigDecimal(request, "amount");

            Transfer transfer = accountService.transfer(sourceUUID, targetUUID, amount);

            return json(transfer);

        });

    }

    /**
     * Return an instance of {@link BigDecimal} from the request.
     *
     * @param request   {@link Request} instance
     * @param parameter {@link String} parameter name
     * @return {@link BigDecimal} instance
     */
    private BigDecimal getBigDecimal(Request request, String parameter) {

        String input = request.params(parameter);

        return NumberUtil.getBigDecimal(input);

    }

}
