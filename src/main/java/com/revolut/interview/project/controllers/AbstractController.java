package com.revolut.interview.project.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Base class for controllers.
 */
public class AbstractController {

    /**
     * Transform the given object to JSON using the {@link Gson} third party library.
     *
     * @param input {@link Object} any object
     * @return {@link String} JSON string representation
     */
    public String json(Object input) {

        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        return gson.toJson(input);

    }

}
