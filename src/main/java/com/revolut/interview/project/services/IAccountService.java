package com.revolut.interview.project.services;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.entities.Transfer;

import java.math.BigDecimal;

/**
 * Any class annotated with this interface will provide account service methods.
 */
public interface IAccountService {

    Account findAccount(String uuid);

    Account createAccount(BigDecimal balance);

    Transfer transfer(String sourceUUID, String targetUUID, BigDecimal amount);

}
