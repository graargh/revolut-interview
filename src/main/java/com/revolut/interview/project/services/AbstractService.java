package com.revolut.interview.project.services;

import com.revolut.interview.project.exceptions.TransactionActiveException;
import com.revolut.interview.project.exceptions.TransactionNotFoundException;
import com.revolut.interview.project.utils.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Base class for services.
 */
public class AbstractService {

    protected EntityManager entityManager = getEntityManager();

    protected EntityTransaction transaction;

    /**
     * Return the {@link EntityManager} for database related operations.
     *
     * @return
     */
    public EntityManager getEntityManager() {
        return HibernateUtil.getSessionFactory().createEntityManager();
    }

    /**
     * Start a transaction.
     */
    public void startTx() {

        if (transaction == null) {
            transaction = entityManager.getTransaction();
        }

        if (transaction.isActive()) {
            throw new TransactionActiveException();
        }

        transaction.begin();

    }

    /**
     * Commit the active transaction.
     */
    public void commitTx() {

        if (transaction == null) {
            throw new TransactionNotFoundException();
        }

        transaction.commit();

    }

    /**
     * Rollback the active transaction.
     */
    public void rollbackTx() {

        if (transaction == null) {
            throw new TransactionNotFoundException();
        }

        transaction.rollback();

    }

}
