package com.revolut.interview.project.services.impl;

import com.revolut.interview.project.entities.Account;
import com.revolut.interview.project.entities.Transfer;
import com.revolut.interview.project.exceptions.AccountCreationException;
import com.revolut.interview.project.exceptions.AccountNotFoundException;
import com.revolut.interview.project.exceptions.TransferBetweenAccountsException;
import com.revolut.interview.project.services.AbstractService;
import com.revolut.interview.project.services.IAccountService;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigDecimal;

/**
 * Primary service class for the project.
 * Provide methods for interacting with accounts.
 */
public class AccountService extends AbstractService implements IAccountService {

    /**
     * Create an {@link Account} with the given balance.
     *
     * @param balance {@link BigDecimal} instance
     * @return {@link Account} instance
     */
    @Transactional(Transactional.TxType.MANDATORY)
    public Account createAccount(BigDecimal balance) {

        try {

            startTx();

            Account account = new Account(balance);

            entityManager.persist(account);

            commitTx();

            return account;

        } catch (Exception ex) {
            rollbackTx();
            throw new AccountCreationException();
        }

    }

    /**
     * Transfer balance from one account to another one.
     *
     * @param sourceUUID {@link String} source account UUID attribute
     * @param targetUUID {@link String} target account UUID attribute
     * @param amount     {@link BigDecimal} amount to send
     * @return {@link Transfer} represents the transaction
     */
    @Override
    public Transfer transfer(String sourceUUID, String targetUUID, BigDecimal amount) {

        try {

            startTx();

            Account source = findAccount(sourceUUID);

            Account target = findAccount(targetUUID);

            Transfer transfer = source.transfer(amount, target);

            entityManager.merge(source);

            entityManager.merge(target);

            entityManager.persist(transfer);

            commitTx();

            return transfer;

        } catch (Exception ex) {
            rollbackTx();
            throw new TransferBetweenAccountsException();
        }

    }

    /**
     * Return an {@link Account} with the given UUID attribute.
     *
     * @param uuid {@link String} instance
     * @return {@link Account} instance
     */
    public Account findAccount(String uuid) {

        try {

            String queryString = "SELECT account FROM Account account WHERE account.uuid = :uuid";

            Query query = entityManager
                    .createQuery(queryString)
                    .setParameter("uuid", uuid);

            return (Account) query.getSingleResult();

        } catch (NoResultException ex) {
            throw new AccountNotFoundException();
        }

    }

}
