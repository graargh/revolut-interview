package com.revolut.interview.project.constants;

/**
 * Define constants that are used application-wide.
 */
public abstract class ApplicationConstants {

    public static final Integer DEFAULT_PORT = 8080;

    public static final Integer BALANCE_SCALE = 2;

    public static final String HEALTH_CHECK_RESPONSE = "ok";

    public static final String HEALTH_CHECK_ENDPOINT = "/health-check";

    public static final String ACCOUNT_ADD_ENDPOINT = "/account/:balance";

    public static final String ACCOUNT_FIND_ENDPOINT = "/account/:uuid";

    public static final String ACCOUNT_TRANSFER_ENDPOINT = "/account/:sourceUUID/transfer/:amount/:targetUUID";

}
