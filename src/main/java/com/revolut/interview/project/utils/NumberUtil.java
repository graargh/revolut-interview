package com.revolut.interview.project.utils;

import com.revolut.interview.project.constants.ApplicationConstants;
import com.revolut.interview.project.exceptions.InvalidAmountFormatException;

import java.math.BigDecimal;

public class NumberUtil {

    /**
     * Return a {@link BigDecimal} instance if the given string is a valid number.
     *
     * @param input {@link String} any given string
     * @return {@link BigDecimal} instance or {@link InvalidAmountFormatException}
     */
    public static BigDecimal getBigDecimal(String input) {

        try {
            return new BigDecimal(input).setScale(ApplicationConstants.BALANCE_SCALE, BigDecimal.ROUND_DOWN);
        } catch (Exception ex) {
            throw new InvalidAmountFormatException();
        }


    }

}
