package com.revolut.interview.project.utils;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    /**
     * Return an instance of the Hibernate {@link SessionFactory}.
     *
     * @return {@link SessionFactory} instance
     */
    public static SessionFactory getSessionFactory() {

        if (sessionFactory == null) {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        }

        return sessionFactory;

    }

}
