# Revolut Interview

## Third party libraries used

* [H2](http://www.h2database.com/html/main.html): provide in-memory database;
* [SLF4J](https://www.slf4j.org): widely used logging library;
* [Guice](https://github.com/google/guice): provide dependency injection;
* [Spark](http://sparkjava.com): micro framework for creating endpoints that can run in an embedded server (jetty);
* [Gson](https://github.com/google/gson): framework to serialize/deserialize objects from/to JSON;
* [Hibernate](http://hibernate.org): ORM mapping / database integration;
* [Gradle](https://gradle.org/): build automation and dependency  manager;
* [Gradle Shadow](https://github.com/johnrengelman/shadow): plugin for creating fat JARs;

## Architectural decisions

After reading **requirement 4** (result should be a standalone program) my first thought was to use `Spring Boot`, although that idea was discarded in attention to **requirement 2** (springless).

Another possible option was to write a main class that would download all the necessary containers (docker or maybe jetty) and setup/bootstrap everything, but that could get really messy.

After some research I have found `Spark` and after a quick test I thought it would be the best/quickest solution available. It runs in an embedded server out of the box and is as quick to use / learn as NodeJS `Express`.

Since `Spark` only provides the mechanism necessary for setting up the endpoints without any support for database integration, `Hibernate` was used to fill that gap.

`Guice` was used to provide a quick way of injecting dependencies (since we are not using Spring/J2EE).

## Endpoints

Following are the endpoints created for the project:

* `/health-check` (GET): returns a simple string response to be used for checking if the server is up;
* `/account/:balance` (PUT): add a new account with the given balance;
* `/account/:uuid` (GET): return the account with the given UUID;
* `/account/:sourceUUID/transfer/:amount/:targetUUID` (POST): transfer :amount from source account to target account;

## Instructions

Following are instructions to see the project in action:

* Import the `build.grade` file into a IDE (personally I have used Intellij IDEA);
* The task `shadowDistZip` creates the fat JAR (executable JAR with all the dependencies);
* Test suite for unit/integration tests is provided by `com.revolut.interview.project.InterviewSuites`;